import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FrontPageComponent} from './pages/front-page/front-page.component';
import {LoginPageComponent} from './pages/login-page/login-page.component';
import {CreatePageComponent} from './pages/create-page/create-page.component';
import {TrainerSelectedGuard} from './services/trainer-selected.guard';
import {CataloguePageComponent} from './pages/catalogue-page/catalogue-page.component';
import {DetailsPageComponent} from './pages/details-page/details-page.component';

const routes: Routes = [
  {
    path: '',
    component: FrontPageComponent,
    canActivate: [TrainerSelectedGuard],
    children: [
    ]
  },
  {
    path: 'catalogue',
    component: CataloguePageComponent,
    canActivate: [TrainerSelectedGuard],
  },
  {
    path: 'details/:id',
    component: DetailsPageComponent,
    canActivate: [TrainerSelectedGuard],
  },
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: 'create',
    component: CreatePageComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
