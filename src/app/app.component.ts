import {Component, OnInit} from '@angular/core';
import {TrainerService} from './services/trainer/trainer.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'pokemontrainer';
  isNavbarCollapsed = true;

  constructor(private trainerService: TrainerService, private router: Router) {
  }

  logOut(){
    this.trainerService.logOut();
    this.router.navigate(['/login']);
  }

  ngOnInit(): void {
  }
}
