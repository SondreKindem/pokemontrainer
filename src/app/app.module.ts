import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CreatePageComponent} from './pages/create-page/create-page.component';
import {FrontPageComponent} from './pages/front-page/front-page.component';
import {LoginPageComponent} from './pages/login-page/login-page.component';
import {BackgroundImageComponent} from './components/background-image/background-image.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TrainerSummaryComponent} from './components/trainer-summary/trainer-summary.component';
import {CataloguePageComponent} from './pages/catalogue-page/catalogue-page.component';
import {HttpClientModule} from '@angular/common/http';
import {DetailsPageComponent} from './pages/details-page/details-page.component';

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    FrontPageComponent,
    LoginPageComponent,
    CreatePageComponent,
    BackgroundImageComponent,
    TrainerSummaryComponent,
    CataloguePageComponent,
    DetailsPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [],
})
export class AppModule {
}
