export class Pokemon {
  private _id;
  private _name;
  private _sprite: string;
  private _types: Array<any>;
  private _abilities: Array<any>;
  private _stats: {
    attack: number,
    defense: number,
    hp: number,
    'special-attack': number,
    'special-defense': number,
    speed: number,
  };
  private _height: number;
  private _weight: number;
  private _base_experience: number;
  private _moves: Array<string>;

  constructor() {
    this._types = [];
    this._abilities = [];
    this._stats = {
      attack: null,
      defense: null,
      hp: null,
      'special-attack': null,
      'special-defense': null,
      speed: null,
    };
    this._moves = [];
  }

  get id() {
    return this._id;
  }

  set id(value) {
    this._id = value;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
  }

  get sprite(): string {
    return this._sprite;
  }

  set sprite(value: string) {
    this._sprite = value;
  }

  get types(): Array<any> {
    return this._types;
  }

  set types(value: Array<any>) {
    this._types = value;
  }

  get abilities(): Array<any> {
    return this._abilities;
  }

  set abilities(value: Array<any>) {
    this._abilities = value;
  }

  get stats(): { attack: number; defense: number; hp: number; 'special-attack': number; 'special-defense': number; speed: number } {
    return this._stats;
  }

  set stats(value: { attack: number; defense: number; hp: number; 'special-attack': number; 'special-defense': number; speed: number }) {
    this._stats = value;
  }

  get height(): number {
    return this._height;
  }

  set height(value: number) {
    this._height = value;
  }

  get weight(): number {
    return this._weight;
  }

  set weight(value: number) {
    this._weight = value;
  }

  get base_experience(): number {
    return this._base_experience;
  }

  set base_experience(value: number) {
    this._base_experience = value;
  }

  get moves(): Array<string> {
    return this._moves;
  }

  set moves(value: Array<string>) {
    this._moves = value;
  }
}
