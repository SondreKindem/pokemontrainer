import {Component, OnInit} from '@angular/core';
import {Trainer} from '../../interfaces/trainer';
import {TrainerService} from '../../services/trainer/trainer.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css'],
})

export class LoginPageComponent implements OnInit {

  trainers: { [key: string]: Trainer } = this.trainerService.getAllTrainers();

  constructor(private trainerService: TrainerService, private router: Router) { }

  selectTrainer(trainer: Trainer): void {
    console.log("Selected trainer " + trainer.name);
    this.trainerService.selectTrainer(trainer.id);
    this.router.navigate(['']);
  }

  ngOnInit(): void {
  }

}
