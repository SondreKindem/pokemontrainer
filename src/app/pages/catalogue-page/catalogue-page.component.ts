import {Component, OnInit} from '@angular/core';
import {PokemonService} from '../../services/pokemon/pokemon.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue-page.component.html',
  styleUrls: ['./catalogue-page.component.scss']
})
export class CataloguePageComponent implements OnInit {

  pokemon: Array<any>;
  totalItems: number;
  loading: boolean = false;
  page: number = 1;

  constructor(private pokemonService: PokemonService, private router: Router) {
  }

  loadPage() {
    this.loading = true;
    this.pokemonService.getPokemonList(this.page - 1).then(
      data => {
        this.pokemon = data['results'];
        this.loading = false;
        this.totalItems = this.pokemonService.getTotalItems();
      });
  }

  ngOnInit(): void {
    this.loadPage();
  }
}
