import { Component, OnInit } from '@angular/core';
import {TrainerService} from '../../services/trainer/trainer.service';
import {Pokemon} from '../../classes/pokemon';
import {Trainer} from '../../interfaces/trainer';

@Component({
  selector: 'app-front-page',
  templateUrl: './front-page.component.html',
  styleUrls: ['./front-page.component.css']
})
export class FrontPageComponent implements OnInit {

  trainer: Trainer;
  pokemonList: Array<Pokemon>;

  constructor(private trainerService: TrainerService) {
    this.trainer = this.trainerService.getActive();
    this.pokemonList = this.trainer.pokemon.map(this.convertToPokemon);
  }

  convertToPokemon(pokemon){
    return Object.assign(new Pokemon(), pokemon);
  }

  releasePokemon(pokemon: Pokemon){
    this.pokemonList = this.pokemonList.filter(p => p !== pokemon);
    this.trainerService.releasePokemon(this.pokemonList);
  }

  ngOnInit(): void {
    console.log(this.trainer)
  }

}
