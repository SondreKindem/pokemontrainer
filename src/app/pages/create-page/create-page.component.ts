import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TrainerService} from '../../services/trainer/trainer.service';
import {Trainer} from '../../interfaces/trainer';
import {NgModel} from '@angular/forms';
import {Router} from '@angular/router';
import {Pokemon} from '../../classes/pokemon';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss']
})
export class CreatePageComponent implements OnInit {

  trainerImages: Array<string> = this.trainerService.getTrainerImages();
  trainer: Trainer = {id: null, name: null, image: this.trainerImages[0], pokemon: Array<Pokemon>()}

  // ref for scroll menu
  @ViewChild('horizontalScroller') horizontalScroller: ElementRef;

  constructor(private trainerService: TrainerService, private router: Router) {
  }

  createTrainer(){
    this.trainerService.addTrainer(this.trainer);
    this.trainerService.selectTrainer(this.trainer.id);
    this.router.navigate([''])
  }

  scrollLeft(){
    this.horizontalScroller.nativeElement.scrollLeft -= 150;
    console.log(this.horizontalScroller.nativeElement.scrollLeft);
  }

  scrollRight(){
    this.horizontalScroller.nativeElement.scrollLeft += 150;
    console.log(this.horizontalScroller.nativeElement.scrollLeft);
  }

  ngOnInit(): void {
  }

}
