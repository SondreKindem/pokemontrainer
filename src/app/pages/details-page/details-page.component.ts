import {Component, OnInit} from '@angular/core';
import {TrainerService} from '../../services/trainer/trainer.service';
import {PokemonService} from '../../services/pokemon/pokemon.service';
import {ActivatedRoute} from '@angular/router';
import { Location } from '@angular/common';
import {Pokemon} from '../../classes/pokemon';
import {Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.scss']
})
export class DetailsPageComponent implements OnInit {

  pokemon: Pokemon = new Pokemon();
  filter = new FormControl();
  moves: Observable<any>;
  activeTab: number = 1;

  constructor(
    private trainerService: TrainerService,
    private pokemonService: PokemonService,
    private route: ActivatedRoute,
    private location: Location
  ) {
  }

  filterMoves(moves: string[], text): any[] {
    if (this.filter.value) {
      return moves.filter(move => move.toLowerCase().includes(text.toLowerCase()));
    } else {
      return moves;
    }
  }

  capturePokemon(){
    if(this.pokemon.name)  // Make sure the current pokemon is initialized
      this.trainerService.addPokemon(this.pokemon);
    console.log(this.trainerService.getActive())
  }

  goBack(){
    this.location.back();
  }

  ngOnInit() {
    this.pokemonService.getPokemon(this.route.snapshot.paramMap.get('id')).then(
      pokemon => {
        this.pokemon = pokemon;

        // Set up filtering
        this.moves = this.filter.valueChanges.pipe(
          startWith(''),
          map(text => this.filterMoves(this.pokemon.moves, text))
        );
      }
    );

  }
}
