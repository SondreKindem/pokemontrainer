import {Pokemon} from '../classes/pokemon';

export interface Trainer {
  id: string;
  name: string;
  image: string;
  pokemon: Array<Pokemon>;
}
