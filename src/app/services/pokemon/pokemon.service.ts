import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Pokemon} from '../../classes/pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private totalItems;

  private pokemonListCache = {};
  private pokemonDetailsCache = {};

  constructor(private http: HttpClient) {
    this.pokemonListCache = JSON.parse(sessionStorage.getItem('pokemonListCache')) ?? {};
    this.pokemonDetailsCache = JSON.parse(sessionStorage.getItem('pokemonDetailsCache')) ?? {};
  }

  private async fetchPokemonDetails(url): Promise<Pokemon> {
    // Fetch the pokemon and fill in the properties
    const pokemonResult: object = await this.http.get(url).toPromise();
    const pokemon = new Pokemon();

    pokemon.id = pokemonResult['id'];
    pokemon.name = pokemonResult['name'];
    pokemon.sprite = pokemonResult['sprites']['front_default'];
    pokemon.height = pokemonResult['height'] / 10; // the json does not have decimals
    pokemon.weight = pokemonResult['weight'] / 10;
    pokemon.base_experience = pokemonResult['base_experience'];

    for (const type of pokemonResult['types']) {
      pokemon.types.push(type.type.name);
    }

    for (const ability of pokemonResult['abilities']) {
      pokemon.abilities.push({name: ability.ability.name, hidden: ability.is_hidden});
    }

    for (const stat of pokemonResult['stats']) {
      pokemon.stats[stat.stat.name] = stat.base_stat;
    }

    for (const move of pokemonResult['moves']) {
      pokemon.moves.push(move.move.name);
    }

    this.savePokemonDetailsToCache(pokemon);
    return pokemon;
  }

  private async fetchPokemonList(offset: number = 0): Promise<object> {
    const url = 'https://pokeapi.co/api/v2/pokemon/?limit=20&offset=' + offset;
    const data: object = await this.http.get(url).toPromise();
    console.log('CALLED API');

    for (const pokemon of data['results']) {
      // Get pokemon details and use to fill in required properties
      // Because we fetch the entire pokemon, we might as well cache the pokemon itself while we are at it
      const pokemonDetails = await this.fetchPokemonDetails(pokemon['url']);

      pokemon.sprite = pokemonDetails.sprite;

      pokemon.id = pokemonDetails.id;
      pokemon.types = [];
      for (const type of pokemonDetails.types) {
        pokemon.types.push(type);
      }
    }

    this.savePokemonListToCache(data, offset);

    // Set amount of items, always update in case cache was outdated
    this.totalItems = data['count'];
    return data;
  }

  private savePokemonDetailsToCache(pokemon) {
    try {
      // save page in the in-memory cache
      this.pokemonDetailsCache[pokemon['id']] = pokemon;
      // save all pages in browser session cache
      sessionStorage.setItem('pokemonDetailsCache', JSON.stringify(this.pokemonDetailsCache));
    } catch (err) {
      if (err) {
        console.log(err);
      }
      if (err.name === 'QuotaExceededError') {
        sessionStorage.clear();
      }
    }
  }

  private savePokemonListToCache(data, offset) {
    try {
      // save page in the in-memory cache
      this.pokemonListCache[offset] = data;
      // save all pages in browser session cache
      sessionStorage.setItem('pokemonListCache', JSON.stringify(this.pokemonListCache));
    } catch (err) {
      if (err) {
        console.log(err);
      }
      if (err.name === 'QuotaExceededError') {
        sessionStorage.clear();
      }
    }
  }

  async getPokemonList(page): Promise<object> {
    const offset = page * 20;
    // Return page from cache if it exists
    if (this.pokemonListCache[offset]) {

      if (!this.totalItems) {
        // Set the total amount of items - could be outdated because the data is cached
        this.totalItems = this.pokemonListCache[offset]['count'];
      }

      return this.pokemonListCache[offset];
    } else {
      return this.fetchPokemonList(offset);
    }
  }

  async getPokemon(id): Promise<Pokemon> {
    if (this.pokemonDetailsCache[id]) {
      // Convert json object to Pokemon
      return Object.assign(new Pokemon(), this.pokemonDetailsCache[id]);
    } else {
      return this.fetchPokemonDetails('https://pokeapi.co/api/v2/pokemon/' + id);
    }
  }

  getTotalItems() {
    return this.totalItems;
  }
}


/* Alternative approach with observables
    return this.http.get(url)
      .pipe(
        tap(async data => {

          // Get pokemon image and types -
          // could alternatively get the image by slicing the id from the pkmn url
          data["results"].map(async pokemon => {
            const pokemonResult = await this.http.get(pokemon["url"]).toPromise();
            pokemon.sprite = pokemonResult["sprites"]["front_default"];

            pokemon.types = [];
            for(const type of pokemonResult["types"]){
              pokemon.types.push(type.type.name)
            }
          });

          try {
            // save page in the in-memory cache
            this.pages[offset] = data;
            // save all pages in browser session cache
            sessionStorage.setItem('pokemonCache', JSON.stringify(this.pages));
          } catch (exception) {
            console.log(exception);
          }

          // Set amount of items, always update in case cache was outdated
          this.totalItems = data['count'];

          }, error => console.log(error)
        ),
        share()
      );
     */
