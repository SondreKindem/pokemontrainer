import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {TrainerService} from './trainer/trainer.service';

@Injectable({
  providedIn: 'root'
})

/**
 * The trainerSelectedGuard redirects to the login page if no trainer is selected
 */
export class TrainerSelectedGuard implements CanActivate {
  constructor(private trainerService: TrainerService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.trainerService.getActive()) {
      this.router.navigate(['login']);
    }
    return true;
  }
}
