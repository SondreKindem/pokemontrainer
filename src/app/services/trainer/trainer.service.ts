import {Injectable} from '@angular/core';
import {Trainer} from '../../interfaces/trainer';
import {Pokemon} from '../../classes/pokemon';
import {generate} from 'shortid';

@Injectable({
  providedIn: 'root'
})
/**
 * The TrainerService handles storing, retrieving, and keeping track of the trainer(s).
 */
export class TrainerService {

  private activeTrainer: string;

  private readonly allTrainers: { [key: string]: Trainer };

  private trainerImages: Array<string> = [
    'assets/trainers/trainer1.png',
    'assets/trainers/trainer2.png',
    'assets/trainers/trainer3.png',
    'assets/trainers/trainer4.png',
    'assets/trainers/trainer5.png',
  ];

  constructor() {
    this.allTrainers = JSON.parse(localStorage.getItem('trainers')) ?? {};
    this.activeTrainer = JSON.parse(localStorage.getItem('lastTrainer'));
  }

  /**
   * Return all trainers
   */
  public getAllTrainers(): { [key: string]: Trainer } {
    return this.allTrainers;
  }

  public addTrainer(trainer: Trainer) {
    trainer.id = generate();
    this.allTrainers[trainer.id] = trainer;
    this.saveAllTrainers();
  }

  public selectTrainer(id: string): boolean {
    if (this.allTrainers.hasOwnProperty(id)) {
      this.activeTrainer = id;
      localStorage.setItem('lastTrainer', JSON.stringify(id));
      return true;
    }
    return false;
  }

  public addPokemon(pokemon: Pokemon) {
    this.allTrainers[this.activeTrainer].pokemon.push(pokemon);
    this.saveAllTrainers();
  }

  /*
   * Hitting the limit of working with objects. The pokemon in the trainers loaded from json are not Pokemon instances
   * Might be better to make trainer into a class and use class-transformer to serialize and deserialize
   */
  /**
   * Replace the pokemon array of the trainer
   * @param pokemonArray the new list of pokemon for the current trainer
   */
  public releasePokemon(pokemonArray: Pokemon[]) {
    this.allTrainers[this.activeTrainer].pokemon = pokemonArray;
    this.saveAllTrainers();
  }

  private saveAllTrainers() {
    localStorage.setItem('trainers', JSON.stringify(this.allTrainers));
  }

  /**
   * Return the current trainer
   */
  public getActive(): Trainer {
    return this.allTrainers[this.activeTrainer] ?? null;
  }

  /**
   * Get a list of available trainer profile images
   */
  public getTrainerImages(): Array<string> {
    return this.trainerImages;
  }

  public logOut(){
    this.activeTrainer = null;
    localStorage.setItem('lastTrainer', null);
  }
}
