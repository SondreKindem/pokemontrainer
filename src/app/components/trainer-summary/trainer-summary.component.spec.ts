import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainerSummaryComponent } from './trainer-summary.component';

describe('TrainerSummaryComponent', () => {
  let component: TrainerSummaryComponent;
  let fixture: ComponentFixture<TrainerSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrainerSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainerSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
