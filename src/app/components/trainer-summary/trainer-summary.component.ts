import {Component, Input, OnInit} from '@angular/core';
import {Trainer} from '../../interfaces/trainer';

@Component({
  selector: 'app-trainer-summary',
  templateUrl: './trainer-summary.component.html',
  styleUrls: ['./trainer-summary.component.scss']
})
export class TrainerSummaryComponent implements OnInit {
  @Input() trainer: Trainer;

  constructor() { }

  ngOnInit(): void {
  }

}
