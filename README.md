# PokemonTrainer

I might have spent too much time on the login and trainer card (totally worth it).

Go catch ‘em all!

## Development server

Run `ng serve` & navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
